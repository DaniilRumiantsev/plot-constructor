#ifndef PLOTPROVIDER_H
#define PLOTPROVIDER_H

#define SET_FREQUENCY "Установить частоту"
#define SET_CONSTANT "Установить точное значение"

#include <QCheckBox>
#include <QComboBox>
#include <QDoubleValidator>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMainWindow>
#include <QPushButton>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>

#include <sstream>
#include <windows.h>
#include <conio.h>
#include <tchar.h>

#include "plot.h"

class PlotProvider : public QWidget
{
    Q_OBJECT

private:

    QTimer* timer;

    // Элементы панели управления параметрами

    QComboBox* AOptions;
    QLineEdit* AFrequencyInput;
    QLineEdit* AConstantInput;

    QComboBox* BOptions;
    QLineEdit* BFrequencyInput;
    QLineEdit* BConstantInput;

    QComboBox* COptions;
    QLineEdit* CFrequencyInput;
    QLineEdit* CConstantInput;

    QCheckBox* drawPlotOption;
    QCheckBox* writePlotOption;

    QPushButton* stopButton;
    QPushButton* launchButton;
    QLabel* message;

    // Элементы панели текущего состояния

    QLabel* AState;
    QLabel* BState;
    QLabel* CState;
    QLabel* tState;
    QLabel* YState;

    // Элементы панели действий

    QLabel *chosenFile;
    QPushButton *chooseFileButton;

    // Абстрактная модель графика

    Plot* plot;

    // Управление файлом, отображаемым в память

    HANDLE mapping;
    wchar_t* mappingData;

    /**
     * @brief chooseFileForRecord
     * Открывает проводник для выбора тектового файла под запись графика
     */
    void chooseFileForRecord();

    /**
     * @brief launchPlot
     * Запускает вычисление функции по нажатию на кнопку launchPlotButton
     */
    void launchPlot();

    /**
     * @brief notify Показывает пользователю сообщение красным цветом
     * @param msg Сообщение
     */
    void notify(QString msg);

    /**
     * @brief provideInterface
     * Создаёт и размещает на экране необходимые кнопки, поля ввода, метки
     * и прочие элементы GUI
     */
    void provideInterface();

    /**
     * @brief setHandlers
     * Устанавливает обработчики на кнопки и другие элементы управления
     */
    void setHandlers();

    /**
     * @brief stopPlot Останавливает вычисление функции по нажатию на
     * кнопку stopPlotButton
     */
    void stopPlot();

public:
    PlotProvider(QWidget *parent = 0);
    ~PlotProvider();

    /**
     * @brief drawState
     * Записывает значения (t, Y) в файл, отображаемый в память
     * для дальнейшего рисования графика внешним процессом
     */
    void drawState();

    /**
     * @brief printState
     * Печатает на экране текущие значения параметров и вызывает
     * функцию drawState()
     */
    void printState();
};

#endif // PLOTPROVIDER_H
