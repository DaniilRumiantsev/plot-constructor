#include "plotprovider.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PlotProvider w;
    w.show();

    return a.exec();
}
