#ifndef PLOT_H
#define PLOT_H

# define M_PI 3.14159265358979323846

#include <chrono>
#include <cmath>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <memory>
#include <string>
#include <thread>
#include <vector>


class Plot
{

private:

    // Флаг, показывающий, приостановлено ли вычисление значения функции
    bool paused;
    // Флаг, показывающий, идёт ли запись графика в файл
    bool recorded;
    // Флаг, показывающий, что таймер уже запущен.
    // Используется для защиты от новой инициализации таймера,
    // если пользователь приостановил, а потом возобновил вычисления
    bool timerStarted;

    // Время запуска программы - точка начала отсчёта
    std::chrono::time_point<std::chrono::system_clock> initialTime;
    // Поток для управления записью графика в файл
    std::fstream fs;
    // Массив значений, которые будут записаны в файл
    std::vector<double> values;

    // Количество секунд, прошедшее с начала запуска программы
    // до возобновления после последней паузы
    double localStart;
    // Ьекущие значения коэффициентов и функции
    double A, B, C, Y;
    // Ограничения на коэффициенты
    double AMin, AMax, BMin, BMax, CMin, CMax;
    // Частоты коэффициентов, задаваемые оператором
    double AFrequency, BFrequency, CFrequency;
    // Константные значения коэффициентов, задаваемые оператором
    double AConstant, BConstant, CConstant;
    // Диапазон возможных значений функций при текущих параметрах
    double rangeMin, rangeMax;

    /**
     * @brief setRange
     * Вычисляет диапазон возможных значений функций при текущих параметрах
     */
    void setRange();

public:

    Plot();
    ~Plot();

    /**
     * @brief calculateA
     * Вычисляет значение коэффициента A в текущий момент времени
     */
    void calculateA();

    /**
     * @brief calculateB
     * Вычисляет значение коэффициента B в текущий момент времени
     */
    void calculateB();

    /**
     * @brief calculateC
     * Вычисляет значение коэффициента C в текущий момент времени
     */
    void calculateC();

    /**
     * @brief calculateY
     * Вычисляет значение функции Y в текущий момент времени
     */
    void calculateY();

    /**
     * @brief getA Возыращает текущее значение параметра A
     * @return текущее значение параметра A
     */
    double getA();

    /**
     * @brief getB Возыращает текущее значение параметра B
     * @return текущее значение параметра B
     */
    double getB();

    /**
     * @brief getC Возыращает текущее значение параметра C
     * @return текущее значение параметра C
     */
    double getC();

    /**
     * @brief getTime Возыращает текущее количество секунд,
     * прошедших с момента запуска программы
     * @return количество секунд, прошедшее с момента запуска программы
     */
    double getTime();

    /**
     * @brief getC Возыращает текущее значение функции Y
     * @return текущее значение функции Y
     */
    double getY();

    /**
     * @brief isFileSet проверяет, задан ли файл для записи графика
     * в виде текста на диск
     * @return true, если файл для записи задан, false - иначе
     */
    bool isFileSet();

    /**
     * @brief isRecorded Проверяет, нужно ли записывать график в
     * физический текстовый файл на диске
     * @return true, если запись в файл идёт,
     * false, если запись остановлена или не начата
     */
    bool isRecorded();

    /**
     * @brief runThreadA
     * Запускает поток, управляющий изменением значения коэффциента A
     */
    void runThreadA();

    /**
     * @brief runThreadB
     * Запускает поток, управляющий изменением значения коэффциента B
     */
    void runThreadB();

    /**
     * @brief runThreadC
     * Запускает поток, управляющий изменением значения коэффциента C
     */
    void runThreadC();

    /**
     * @brief runThreadA
     * Запускает поток, управляющий вычислением функции в текущий момент времени
     */
    void runThreadY();

    /**
     * @brief runThreads
     * Разделяет программу еа потоки, для параллельного вычислнеия
     * каждого коэффициента и значения функции
     */
    void runThreads();

    /**
     * @brief setA Устанавливает значение праметра A
     * @param value новое значение параметра A
     */
    void setA(double value);

    /**
     * @brief setB Устанавливает значение праметра B
     * @param value новое значение параметра B
     */
    void setB(double value);

    /**
     * @brief setC Устанавливает значение праметра C
     * @param value новое значение параметра C
     */
    void setC(double value);

    /**
     * @brief setA Устанавливает частоту праметра A
     * @param value новое значение частоты параметра A
     */
    void setAFrequency(double value);

    /**
     * @brief setB Устанавливает частоту праметра B
     * @param value новое значение частоты параметра B
     */
    void setBFrequency(double value);

    /**
     * @brief setC Устанавливает частоту праметра C
     * @param value новое значение частоты параметра C
     */
    void setCFrequency(double value);

    /**
     * @brief setFileForRecord Назначет текстовый файл для записи графика
     * @param path Путь к файлу
     */
    void setFileForRecord(std::string path);

    /**
     * @brief saveData Записывает массив накопленных значений в объекте values
     * в текстовый файл, управляемый потоком fs, в виде графика
     */
    void saveData();

    /**
     * @brief start Начинает или возобновляет процесс вычисления значений функции
     * @param r флаг, указывающий, нужно ли записывать значения
     * в текстовый файл в виде графика
     */
    void start(bool r = false);

    /**
     * @brief stop Останавливает процесс вычисления значений функции
     */
    void stop();

    /**
     * @brief write Осуществляет запись текущих значений (t, Y)
     * в массив values для последкющей записи в тексовый поток
     */
    void write();
};

#endif // PLOT_H
