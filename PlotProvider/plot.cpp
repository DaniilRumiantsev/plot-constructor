#include "plot.h"

Plot::Plot()
{
    AFrequency = BFrequency = CFrequency = localStart = 0;
    paused = recorded = timerStarted = false;

    AMin = 3;
    AMax = 7;
    BMin = 1;
    BMax = 3;
    CMin = 1;
    CMax = 10;

    A = AMin;
    B = BMin;
    C = CMin;

    values = std::vector<double>();
    setRange();
}

Plot::~Plot()
{
    if (fs.is_open()) fs.close();
}

void Plot::calculateA()
{
    /*
     * Если частота не задана, применяем константное значение
     */

    if (AFrequency == 0)
    {
        A = AConstant;
        return;
    }

    /*
     * Расчитываем текущее значение параметра A по формуле
     * A = f(t) = k*sin(w*t) + l, где k, l - коэффициенты,
     * которые позволяют от области значений синуса, равной [-1; 1],
     * перейти к области значений [AMin; AMAax],
     * t - разница между текущим и начальным временем,
     * w - циклическая частота
     */

    double k = (AMax - AMin)/2;
    double l = AMax - k;
    double w = 2*M_PI*AFrequency;
    std::chrono::duration<double> t = std::chrono::system_clock::now() - initialTime;

    A = k*sin(w*t.count()) + l;
}

void Plot::calculateB()
{
    /*
     * Если частота не задана, применяем константное значение
     */

    if (BFrequency == 0)
    {
        B = BConstant;
        return;
    }

    /*
     * Расчитываем текущее значение параметра B по формуле
     * B = f(t) = k*sin(w*t) + l, где k, l - коэффициенты,
     * которые позволяют от области значений синуса, равной [-1; 1],
     * перейти к области значений [BMin; BMAax],
     * t - разница между текущим и начальным временем,
     * w - циклическая частота
     */

    double k = (BMax - BMin)/2;
    double l = BMax - k;
    double w = 2*M_PI*BFrequency;
    std::chrono::duration<double> t = std::chrono::system_clock::now() - initialTime;

    B = k*sin(w*t.count()) + l;
}

void Plot::calculateC()
{
    /*
     * Если частота не задана, применяем константное значение
     */

    if (CFrequency == 0)
    {
        C = CConstant;
        return;
    }

    /*
     * Расчитываем текущее значение параметра C по формуле
     * C = f(t) = k*sin(w*t) + l, где k, l - коэффициенты,
     * которые позволяют от области значений синуса, равной [-1; 1],
     * перейти к области значений [CMin; CMAax],
     * t - разница между текущим и начальным временем,
     * w - циклическая частота
     */

    double k = (CMax - CMin)/2;
    double l = CMax - k;
    double w = 2*M_PI*CFrequency;
    std::chrono::duration<double> t = std::chrono::system_clock::now() - initialTime;

    C = k*sin(w*t.count()) + l;
}

void Plot::calculateY()
{
    /*
     * Рассчитываем текущее значение функции Y по формуле
     * Y = A + B + sin(C*t), где t - разница между текущим и начальным временем
     */

    std::chrono::duration<double> t = std::chrono::system_clock::now() - initialTime;

    Y = A + B + sin(C*t.count());
}

double Plot::getA()
{
    return A;
}

double Plot::getB()
{
    return B;
}

double Plot::getC()
{
    return C;
}

double Plot::getTime()
{
    std::chrono::duration<double> t = std::chrono::system_clock::now() - initialTime;
    return t.count();
}

double Plot::getY()
{
    return Y;
}

bool Plot::isFileSet()
{
    return fs.is_open();
}

bool Plot::isRecorded()
{
    return recorded;
}

void Plot::runThreadA()
{
    while (!paused)
    {
        calculateA();
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

void Plot::runThreadB()
{
    while (!paused)
    {
        calculateB();
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

void Plot::runThreadC()
{
    while (!paused)
    {
        calculateC();
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

void Plot::runThreadY()
{
    while (!paused)
    {
        calculateY();
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}


void Plot::runThreads()
{
    std::thread t1(&Plot::runThreadA, this);
    std::thread t2(&Plot::runThreadB, this);
    std::thread t3(&Plot::runThreadC, this);
    std::thread t4(&Plot::runThreadY, this);
    t1.detach();
    t2.detach();
    t3.detach();
    t4.detach();

    if (recorded)
    {
        std::thread t5(&Plot::write, this);
        t5.detach();
    }
}

void Plot::setA(double value)
{
    if (value < AMin) value = AMin;
    else if (value > AMax) value = AMax;
    AConstant = value;
    AFrequency = 0;
    setRange();
}

void Plot::setB(double value)
{
    if (value < BMin) value = BMin;
    else if (value > BMax) value = BMax;
    BFrequency = 0;
    BConstant = value;
    setRange();
}

void Plot::setC(double value)
{
    if (value < CMin) value = CMin;
    else if (value > CMax) value = CMax;
    CFrequency = 0;
    CConstant = value;
}

void Plot::setAFrequency(double value)
{
    AFrequency = value;
}

void Plot::setBFrequency(double value)
{
    BFrequency = value;
}

void Plot::setCFrequency(double value)
{
    CFrequency = value;
}

void Plot::setFileForRecord(std::string path)
{
    fs.open(path, std::fstream::out);
}

void Plot::setRange()
{
    rangeMin = AMin + BMin - 1;
    rangeMax = AMax + BMax + 1;
}

void Plot::saveData()
{
    size_t pointsCount = values.size();

    fs << std::endl << "   Y" << std::endl;
    fs << "   ^" << std::endl;

    for (double i = rangeMax; i >= rangeMin; i-= 0.25)
    {
        if (i - floor(i) < 0.01)
        {
            fs << std::setw(2) << i << " |";
        }
        else
        {
            fs << "   |";
        }

        for (size_t j = 0; j < pointsCount; ++j)
        {
            fs << (values[j] >= i-0.125 && values[j] < i+0.125 ? "*" : " ");
        }

        fs << std::endl;
    }

    for (double i = rangeMin - 0.25; i >= 0; i -= 0.25)
    {
        if (i - floor(i) < 0.01)
        {
            fs << std::setw(2) << i << " |";
        }
        else
        {
            fs << "   |";
        }

        fs << std::endl;
    }

    fs << "  --";

    for (size_t j = 0; j < pointsCount/4; ++j)
    {
        fs << "---|";
    }

    fs << "-> t" << std::endl << "    ";

    for (size_t j = 0; j < pointsCount/4; ++j)
    {
        fs << std::setw(4) << static_cast<int>(localStart) + j + 1;
    }

    fs << std::endl;
}

void Plot::start(bool r)
{
    paused = false;
    recorded = r;

    if (!timerStarted)
    {
        initialTime = std::chrono::system_clock::now();
        timerStarted = true;
    }

    if (recorded && isFileSet())
    {
        std::chrono::duration<double> t = std::chrono::system_clock::now() - initialTime;
        localStart = t.count();
        fs << "t = " << static_cast<int>(localStart) << std::endl;
    }
}

void Plot::stop()
{
    paused = true;
    recorded = false;
    values.empty();
    saveData();
}

void Plot::write()
{
    if (!fs.is_open()) return;

    while (!paused)
    {
        values.push_back(Y);
        std::this_thread::sleep_for(std::chrono::milliseconds(250));
    }

}
