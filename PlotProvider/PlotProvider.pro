#-------------------------------------------------
#
# Project created by QtCreator 2018-09-29T15:37:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PlotProvider
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++1y -pedantic-errors


SOURCES += main.cpp\
        plotprovider.cpp \
    plot.cpp

HEADERS  += plotprovider.h \
    plot.h
