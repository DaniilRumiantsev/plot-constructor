#include "plotprovider.h"

PlotProvider::PlotProvider(QWidget *parent)
    : QWidget(parent)
{
    plot = new Plot;

    setFixedHeight(600);
    setFixedWidth(800);
    setWindowTitle("Расчёт значения функции по заданным параметрам");

    provideInterface();
}

PlotProvider::~PlotProvider()
{
    if (plot->isRecorded() && plot->isFileSet()) plot->saveData();
    delete plot;
    UnmapViewOfFile(mappingData);
    CloseHandle(mapping);
}

void PlotProvider::chooseFileForRecord()
{
    QString filename = QFileDialog::getOpenFileName(
                this,
                "Выберете файл для записи графика",
                QString(),
                "Text files (*.txt)"
                );

    if (!filename.isEmpty())
    {
        plot->setFileForRecord(filename.toLatin1().data());
        chosenFile->setText(filename);
        writePlotOption->setDisabled(false);
    }
    else
    {
        chosenFile->setText("Файл не выбран");
        writePlotOption->setDisabled(true);
        writePlotOption->setChecked(false);
    }
}

void PlotProvider::drawState()
{

    std::ostringstream ss;
    ss << plot->getTime() << " " << plot->getY() << std::endl;
    std::string data = ss.str();
    std::wstring wdata;
    for (auto c : data) wdata += wchar_t(c);

    wchar_t lpName[] = L"PlotProviderCoo";
    const wchar_t* message = wdata.c_str();

    mapping = CreateFileMapping(
                INVALID_HANDLE_VALUE,
                nullptr,
                PAGE_READWRITE,
                0,
                63,
                lpName
                );

    if (mapping == nullptr)
    {
        return;
    }

    mappingData = (wchar_t*)MapViewOfFile(mapping,
                            FILE_MAP_ALL_ACCESS,
                            0,
                            0,
                            63);

    if (mappingData == nullptr)
    {
        CloseHandle(mapping);
        return;
    }

    CopyMemory((void*)mappingData, message, wdata.size()*2);
    _getch();
}

void PlotProvider::launchPlot()
{
    if (AOptions->currentText() == SET_FREQUENCY && AFrequencyInput->text().isEmpty())
    {
        notify("Установите частоту параметра A");
        return;
    }
    else if (AOptions->currentText() == SET_CONSTANT && AConstantInput->text().isEmpty())
    {
        notify("Установите точное значение параметра A");
        return;
    }
    else if (BOptions->currentText() == SET_FREQUENCY && BFrequencyInput->text().isEmpty())
    {
        notify("Установите частоту параметра B");
        return;
    }
    else if (BOptions->currentText() == SET_CONSTANT && BConstantInput->text().isEmpty())
    {
        notify("Установите точное значение параметра B");
        return;
    }
    else if (AOptions->currentText() == SET_FREQUENCY && AFrequencyInput->text().isEmpty())
    {
        notify("Установите частоту параметра A");
        return;
    }
    else if (COptions->currentText() == SET_CONSTANT && CConstantInput->text().isEmpty())
    {
        notify("Установите точное значение параметра C");
        return;
    }

    notify("Выполняются вычисления...");

    launchButton->setDisabled(true);
    AOptions->setDisabled(true);
    AFrequencyInput->setDisabled(true);
    AConstantInput->setDisabled(true);
    BOptions->setDisabled(true);
    BFrequencyInput->setDisabled(true);
    BConstantInput->setDisabled(true);
    COptions->setDisabled(true);
    CFrequencyInput->setDisabled(true);
    CConstantInput->setDisabled(true);
    stopButton->setDisabled(false);


    if (AOptions->currentText() == SET_FREQUENCY && !AFrequencyInput->text().isEmpty())
    {
        plot->setAFrequency(AFrequencyInput->text().toDouble());
    }

    if (BOptions->currentText() == SET_FREQUENCY && !BFrequencyInput->text().isEmpty())
    {
        plot->setBFrequency(BFrequencyInput->text().toDouble());
    }

    if (COptions->currentText() == SET_FREQUENCY && !CFrequencyInput->text().isEmpty())
    {
        plot->setCFrequency(CFrequencyInput->text().toDouble());
    }

    if (AOptions->currentText() == SET_CONSTANT && !AConstantInput->text().isEmpty())
    {
        plot->setA(AConstantInput->text().toDouble());
    }

    if (BOptions->currentText() == SET_CONSTANT && !BConstantInput->text().isEmpty())
    {
        plot->setB(BConstantInput->text().toDouble());
    }

    if (COptions->currentText() == SET_CONSTANT && !CConstantInput->text().isEmpty())
    {
        plot->setC(CConstantInput->text().toDouble());
    }

    if (drawPlotOption->isChecked())
    {
        std::system("start ./PlotDrawer.exe draw");
    }

    plot->start(writePlotOption->isChecked());
    plot->runThreads();

    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &PlotProvider::printState);
    timer->start(250);
}

void PlotProvider::notify(QString msg)
{
    message->setText(msg);
    message->show();
}

void PlotProvider::provideInterface()
{
    // Поля для управления параметром A

    AOptions = new QComboBox;
    AOptions->addItem(SET_FREQUENCY);
    AOptions->addItem(SET_CONSTANT);

    AFrequencyInput = new QLineEdit;
    AFrequencyInput->setPlaceholderText("Частота, Гц");
    AFrequencyInput->setValidator(new QDoubleValidator(0.1, 100, 1, this));

    AConstantInput = new QLineEdit;
    AConstantInput->setPlaceholderText("Точное значение");
    AConstantInput->setValidator(new QIntValidator(3, 7, this));
    AConstantInput->setDisabled(true);

    QHBoxLayout* AValuesLayout = new QHBoxLayout;
    AValuesLayout->addWidget(new QLabel("A: "));
    AValuesLayout->addWidget(AOptions);
    AValuesLayout->addWidget(AFrequencyInput);
    AValuesLayout->addWidget(AConstantInput);

    QWidget* AValues = new QWidget(this);
    AValues->show();
    AValues->setLayout(AValuesLayout);

    // Поля для управления параметром B

    BOptions = new QComboBox;
    BOptions->addItem(SET_FREQUENCY);
    BOptions->addItem(SET_CONSTANT);

    BFrequencyInput = new QLineEdit;
    BFrequencyInput->setPlaceholderText("Частота, Гц");
    BFrequencyInput->setValidator(new QDoubleValidator(0.1, 100, 1, this));

    BConstantInput = new QLineEdit;
    BConstantInput->setPlaceholderText("Точное значение");
    BConstantInput->setValidator(new QIntValidator(1, 10, this));
    BConstantInput->setDisabled(true);

    QHBoxLayout* BValuesLayout = new QHBoxLayout;
    BValuesLayout->addWidget(new QLabel("B: "));
    BValuesLayout->addWidget(BOptions);
    BValuesLayout->addWidget(BFrequencyInput);
    BValuesLayout->addWidget(BConstantInput);

    QWidget* BValues = new QWidget(this);
    BValues->show();
    BValues->setLayout(BValuesLayout);

    // Поля для управления параметром C

    COptions = new QComboBox;
    COptions->addItem(SET_FREQUENCY);
    COptions->addItem(SET_CONSTANT);

    CFrequencyInput = new QLineEdit;
    CFrequencyInput->setPlaceholderText("Частота, Гц");
    CFrequencyInput->setValidator(new QDoubleValidator(0.1, 100, 1, this));

    CConstantInput = new QLineEdit;
    CConstantInput->setPlaceholderText("Точное значение");
    CConstantInput->setValidator(new QIntValidator(1, 7, this));
    CConstantInput->setDisabled(true);

    QHBoxLayout* CValuesLayout = new QHBoxLayout;
    CValuesLayout->addWidget(new QLabel("C: "));
    CValuesLayout->addWidget(COptions);
    CValuesLayout->addWidget(CFrequencyInput);
    CValuesLayout->addWidget(CConstantInput);

    QWidget* CValues = new QWidget(this);
    CValues->show();
    CValues->setLayout(CValuesLayout);

    // Кнопки управления

    stopButton = new QPushButton("Остановить");
    stopButton->setDisabled(true);

    launchButton = new QPushButton("Начать выполнение");

    QHBoxLayout* ControlButtonsLayout = new QHBoxLayout;
    ControlButtonsLayout->addWidget(stopButton);
    ControlButtonsLayout->addWidget(launchButton);

    QWidget* ControlButtons = new QWidget(this);
    ControlButtons->show();
    ControlButtons->setLayout(ControlButtonsLayout);

    // Опции рисования графика и его записи в текстовый файл

    drawPlotOption = new QCheckBox("Рисовать график в новом окне");
    drawPlotOption->setChecked(false);

    writePlotOption = new QCheckBox("Записывать график в текстовый файл");
    writePlotOption->setChecked(false);
    writePlotOption->setDisabled(true);

    // Виджет, содержащий поля для управления параметрами

    QVBoxLayout* paramsFormLayout = new QVBoxLayout;
    paramsFormLayout->addWidget(new QLabel("Установка параметров и запуск процесса"));
    paramsFormLayout->addWidget(AValues);
    paramsFormLayout->addWidget(BValues);
    paramsFormLayout->addWidget(CValues);
    paramsFormLayout->addWidget(drawPlotOption);
    paramsFormLayout->addWidget(writePlotOption);
    paramsFormLayout->addWidget(ControlButtons);

    QWidget* paramsForm = new QWidget(this);
    paramsForm->setFixedWidth(500);
    paramsForm->show();
    paramsForm->setLayout(paramsFormLayout);

    // Область показа текущих значений

    AState = new QLabel("A = 0");
    BState = new QLabel("B = 0");
    CState = new QLabel("C = 0");
    tState = new QLabel("t = 0");
    YState = new QLabel("Y = 0");

    QVBoxLayout* stateLayout = new QVBoxLayout;
    stateLayout->addWidget(new QLabel("Текущее состояние"));
    stateLayout->addWidget(AState);
    stateLayout->addWidget(BState);
    stateLayout->addWidget(CState);
    stateLayout->addWidget(tState);
    stateLayout->addWidget(YState);

    QWidget* state = new QWidget(this);
    state->setFixedWidth(150);
    state->show();
    state->setLayout(stateLayout);

    // Панель управления параметрами и показа текущих значений

    QHBoxLayout* paramsPanelLayout = new QHBoxLayout;
    paramsPanelLayout->addWidget(paramsForm);
    paramsPanelLayout->addWidget(state);

    QWidget* paramsPanel = new QWidget(this);
    paramsPanel->setFixedHeight(300);
    paramsPanel->show();
    paramsPanel->setLayout(paramsPanelLayout);

    // Панель действий

    chooseFileButton = new QPushButton("Выбрать файл для записи");
    chooseFileButton->setFixedWidth(200);
    chosenFile = new QLabel("Файл не выбран");

    QHBoxLayout* actionButtonsLayout = new QHBoxLayout;
    actionButtonsLayout->addWidget(chooseFileButton);
    actionButtonsLayout->addWidget(chosenFile);

    QWidget* actionButtons = new QWidget(this);
    actionButtons->show();
    actionButtons->setLayout(actionButtonsLayout);

    message = new QLabel;
    message->setStyleSheet("QLabel { color: #ff0000; padding: 20px; }");

    QVBoxLayout* actionPanelLayout = new QVBoxLayout;
    actionPanelLayout->addWidget(message);
    actionPanelLayout->addWidget(new QLabel("Запись графика в текстовый файл"));
    actionPanelLayout->addWidget(actionButtons);

    QWidget* actionPanel = new QWidget(this);
    actionPanel->setContentsMargins(100, 0, 100, 0);
    actionPanel->setFixedHeight(160);
    actionPanel->show();
    actionPanel->setLayout(actionPanelLayout);

    // Корневой шаблон

    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addWidget(paramsPanel);
    mainLayout->addWidget(actionPanel);
    setLayout(mainLayout);
    setHandlers();
}

void PlotProvider::printState()
{
    AState->setText("A = " + QString::number(plot->getA()));
    BState->setText("B = " + QString::number(plot->getB()));
    CState->setText("C = " + QString::number(plot->getC()));
    tState->setText("t = " + QString::number((int)plot->getTime()));
    YState->setText("Y = " + QString::number(plot->getY()));
    repaint();

    drawState();

}

void PlotProvider::setHandlers()
{
    // Выбор частоты/точного значения параметра A

    connect(AOptions, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::activated),
        [=](const QString &text){
        if (text == SET_FREQUENCY)
        {
            AFrequencyInput->setEnabled(true);
            AConstantInput->setDisabled(true);
        }
        else if (text == SET_CONSTANT)
        {
            AFrequencyInput->setEnabled(false);
            AConstantInput->setDisabled(false);
        }
    });

    // Выбор частоты/точного значения параметра B

    connect(BOptions, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::activated),
        [=](const QString &text){
        if (text == SET_FREQUENCY)
        {
            BFrequencyInput->setEnabled(true);
            BConstantInput->setDisabled(true);
        }
        else if (text == SET_CONSTANT)
        {
            BFrequencyInput->setEnabled(false);
            BConstantInput->setDisabled(false);
        }
    });

    // Выбор частоты/точного значения параметра C

    connect(COptions, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::activated),
        [=](const QString &text){
        if (text == SET_FREQUENCY)
        {
            CFrequencyInput->setEnabled(true);
            CConstantInput->setDisabled(true);
        }
        else if (text == SET_CONSTANT)
        {
            CFrequencyInput->setEnabled(false);
            CConstantInput->setDisabled(false);
        }
    });

    // Создание графика
    connect(launchButton, &QPushButton::clicked, this, &PlotProvider::launchPlot);

    // Остановка вычислений
    connect(stopButton, &QPushButton::clicked, this, &PlotProvider::stopPlot);

    // Выбор файла для записи
    connect(chooseFileButton, &QPushButton::clicked, this, &PlotProvider::chooseFileForRecord);
}

void PlotProvider::stopPlot()
{
    notify("Вычисления остановлены");

    plot->stop();
    timer->stop();

    stopButton->setDisabled(true);
    launchButton->setDisabled(false);
    AOptions->setDisabled(false);

    if (AOptions->currentText() == SET_FREQUENCY)
    {
        AFrequencyInput->setDisabled(false);
    }
    else
    {
        AConstantInput->setDisabled(false);
    }

    BOptions->setDisabled(false);

    if (BOptions->currentText() == SET_FREQUENCY)
    {
        BFrequencyInput->setDisabled(false);
    }
    else
    {
        BConstantInput->setDisabled(false);
    }

    COptions->setDisabled(false);

    if (COptions->currentText() == SET_FREQUENCY)
    {
        CFrequencyInput->setDisabled(false);
    }
    else
    {
        CConstantInput->setDisabled(false);
    }

}


