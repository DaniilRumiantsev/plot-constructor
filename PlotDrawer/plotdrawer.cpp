#include "plotdrawer.h"

PlotDrawer::PlotDrawer(QWidget *parent, std::string mode)
    : QWidget(parent)
{
    t = Y = 0;
    stopDraw = false;

    setFixedWidth(1000);
    setFixedHeight(620);

    provideInterface();
    setHandlers();

    // Если при запуске получен агрумент "draw",
    // то сразу рисуем график
    if (!mode.empty() && mode == "draw")
    {
        drawPlot();
    }

}

PlotDrawer::~PlotDrawer()
{

}

void PlotDrawer::paintEvent(QPaintEvent *e)
{
    if (!stopDraw)
    {
        QPainter p;
        p.begin(this);

        // Рисуем оси координат

        QLine tAxis = QLine(35, 580, 960, 580);
        QLine YAxis = QLine(35, 150, 35, 580);

        static const QPointF tArrowPoints[3] = {
            QPointF(951, 570),
            QPointF(961, 580),
            QPointF(951, 590),
        };

        static const QPointF YArrowPoints[3] = {
            QPointF(25, 159),
            QPointF(35, 149),
            QPointF(45, 159),
        };

        p.drawLine(tAxis);
        p.drawLine(YAxis);
        p.drawPolyline(tArrowPoints, 3);
        p.drawPolyline(YArrowPoints, 3);

        p.setFont(QFont("Arial", 12));
        p.drawText(31, 122, 20, 20, 0, "Y");
        p.drawText(976, 568, 20, 20, 0, "t");


        // Подписываем шкалу значений на оси Y

        p.setPen(QColor(30,30,200));

        for (int i = 1; i <= 12; ++i)
        {
            p.drawLine(QLine(33, 580-i*35, 38, 580-i*35));
            p.drawText(4, 574-i*35, 20, 20, 0, QString::number(i));
        }

        // Рисуем график и значения времени по оси t

        p.setPen(QColor(255,30,30));
        p.drawPolyline(scaledPoints.data(), scaledPoints.size());

        p.setPen(QColor(30,30,200));
        p.setFont(QFont("Arial", 8));

        int index = 0;
        for (size_t i = 0; i < points.size(); ++i)
        {
            index = i;
            if (points[i].x() - floor(points[i].x() < 0.1)) break;
        }

        for (size_t i = index; i < points.size(); i+=5)
        {
            p.drawLine(QLine(35+i*7, 577, 35+i*7, 584));
            p.drawText(23+i*7, 595, 28, 20, 0, QString::number(round(points[i].x())));
        }

        p.end();
    }


}

void PlotDrawer::drawPlot()
{
    message->setText("Ожидание получения данных от процесса, вычисляющего значение функции");
    drawPlotButton->hide();
    stopPlotButton->show();

    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &PlotDrawer::waitForData);
    timer->start(200);
    repaint();

}

void PlotDrawer::loadPlot()
{
    QString filename = QFileDialog::getOpenFileName(
                this,
                "Выберете файл для чтения графика",
                QString(),
                "Text files (*.txt)"
                );

    if (!filename.isEmpty())
    {
        message->setText("Выбран файл " + filename);

        std::ifstream fs(filename.toLatin1().data());

        if (!fs.is_open())
        {
            message->setText("Не удаётся открыть файл на чтение");
            return;
        }

        stopDraw = true;
        repaint();

        char c;

        plot->setText("");

        while ((c = fs.get()) != std::char_traits<char>::eof())
        {
            plot->setText(plot->text()+c);
        }

        QScrollArea* scrollArea = new QScrollArea(this);
        scrollArea->setBackgroundRole(QPalette::Light);
        scrollArea->setWidget(plot);
        scrollArea->setGeometry(50, 120, 900, 480);
        scrollArea->show();


        fs.close();
    }
    else
    {
        message->setText("Файл не выбран");
    }
}

void PlotDrawer::provideInterface()
{
    // Кнопки управления

    drawPlotButton = new QPushButton(this);
    drawPlotButton->setText("Нарисовать график в режиме реального времени");
    drawPlotButton->setGeometry(100, 60, 300, 32);


    stopPlotButton = new QPushButton(this);
    stopPlotButton->setText("Остановить отрисовку графика");
    stopPlotButton->setGeometry(100, 60, 300, 32);
    stopPlotButton->hide();

    loadPlotButton = new QPushButton(this);
    loadPlotButton->setText("Нарисовать график по данным из файла");
    loadPlotButton->setGeometry(600, 60, 300, 32);

    // Показ уведомлений

    message = new QLabel(this);
    message->setText("Выберите способ рисования графика");
    message->setGeometry(10, 16, 980, 32);
    message->setAlignment(Qt::AlignCenter);

    // Область рисования графика

    plot = new QLabel;
    QFont f = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    f.setPixelSize(12);
    plot->setFont(f);
    plot->setMinimumHeight(2700);
    plot->setMinimumWidth(20000);
}

void PlotDrawer::setHandlers()
{
    // Рисование графика в режиме реального времени
    connect(drawPlotButton, &QPushButton::clicked, this, &PlotDrawer::drawPlot);
    // Остановка отрисовки графика
    connect(stopPlotButton, &QPushButton::clicked, this, &PlotDrawer::stopPlot);
    // Рисование графика из текстового файла
    connect(loadPlotButton, &QPushButton::clicked, this, &PlotDrawer::loadPlot);
}

void PlotDrawer::stopPlot()
{
    timer->stop();
    drawPlotButton->show();
    stopPlotButton->hide();
    stopDraw = true;
}

void PlotDrawer::waitForData()
{

    wchar_t lpName[] = L"PlotProviderCoo";
    HANDLE mapping;
    const wchar_t* pData;

    mapping = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, lpName);

    if (mapping != nullptr)
    {
        stopDraw = false;

        pData = (const wchar_t*)MapViewOfFile(mapping, FILE_MAP_ALL_ACCESS, 0, 0, 63);

        if (pData == nullptr)
        {
            message->setText("Ошибка получения данных");
            repaint();
            CloseHandle(mapping);
            return;
        }

        std::wstring text(pData);
        std::string str = std::string(text.begin(), text.end());
        std::string::size_type size;
        t = std::stod(str, &size);
        Y = std::stod(str.substr(size));
        message->setText("t = "+QString::number(t)+", Y = "+QString::number(Y));

        // Всего на графике умещается 130 точек при выбранном масштабе.
        // При превышении этого колиечтсва удаляем первую точку, а остальные
        // сдвигаем на одну позицию влево, чтобы получился "бегущий график"

        if (points.size() > 130)
        {
            points.erase(points.begin());
        }

        if (scaledPoints.size() > 130)
        {
            scaledPoints.erase(scaledPoints.begin());
            for (size_t i = 0; i < scaledPoints.size(); ++i) scaledPoints[i].setX(scaledPoints[i].x()-7);
        }

        points.push_back(QPointF(t, Y));
        scaledPoints.push_back(QPointF((t-points.front().x())*35+35, 580-Y*35));

        repaint();
        UnmapViewOfFile(pData);
        CloseHandle(mapping);
    }


}
