#-------------------------------------------------
#
# Project created by QtCreator 2018-09-30T12:06:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PlotDrawer
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++1y -pedantic-errors

SOURCES += main.cpp\
        plotdrawer.cpp

HEADERS  += plotdrawer.h
