#ifndef PLOTDRAWER_H
#define PLOTDRAWER_H

#include <QFileDialog>
#include <QFontDatabase>
#include <QLabel>
#include <QLine>
#include <QPainter>
#include <QPushButton>
#include <QScrollArea>
#include <QTimer>
#include <QWidget>

#include <chrono>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <memory>
#include <string>
#include <sstream>
#include <thread>
#include <vector>

#include <windows.h>
#include <conio.h>
#include <tchar.h>
#include <unistd.h>
#include <iostream>

class PlotDrawer : public QWidget
{
    Q_OBJECT

    // Таймер, отвечающий за периодическое обращение к файлу, отображаемому в память
    QTimer* timer;
    // Массив пар значений (t, Y), полученный от внешнего процесса
    std::vector<QPointF> points;
    // Массив пар значений (t, Y), масшатабированный для отображения в виде графика
    std::vector<QPointF> scaledPoints;

    // Кнопка, по нажатию на которую рисуется график по данным из внешнего процесса
    QPushButton* drawPlotButton;
    // Кнопка приостановки рисования графика
    QPushButton* stopPlotButton;
    // Кнопка загрузки графика из текстового файла
    QPushButton* loadPlotButton;
    // Метка для показа сообщений
    QLabel* message;
    // Метка для отображения графика в виде текста из текмтового файла
    QLabel* plot;

    // Текущие значения времени и функции Y
    double t, Y;
    // Флаг, предписывающий не рисовать график при очередной перерисовке экрана
    bool stopDraw;

    /**
     * @brief drawPlot
     * Рисует графики используя точки (t, Y), полученные функцией waitForData()
     */
    void drawPlot();

    /**
     * @brief loadPlot
     * Загружает график из текстового файла и отображает на экране в
     * виде текста. Никаких проверок на содержимое файла не производит -
     * отобразит содержимое любого файла
     */
    void loadPlot();

    /**
     * @brief stopPlot
     * Останавливает динамическую отрисовку графика на экране
     */
    void stopPlot();

    /**
     * @brief paintEvent
     * Перерисовывает страницу всякий раз, когда происходят
     * требующие этого изменения
     * @param e Не используется
     */
    void paintEvent(QPaintEvent *e);

    /**
     * @brief provideInterface
     * Создаёт необходимые кнопки, метки и прочие элементы GUI
     */
    void provideInterface();

    /**
     * @brief setHandlers
     * Устанавливает обработчики на кнопки
     */
    void setHandlers();

    /**
     * @brief waitForData
     * Пытается получить координаты очередной точки для отрисовки графика
     * через файл, отображаемый в память ынешним процессом. Повторяет попытку
     * 5 раз в секунду
     */
    void waitForData();

public:

    /**
     * @brief PlotDrawer конструктор
     * @param parent виждет-родитель
     * @param mode передаётся первым аргументом при запуске программы.
     * Если значение равно "draw", то будет сразу рисоваться график,
     * получаемый внешним процессом через файл, отображаемый в память
     */
    PlotDrawer(QWidget *parent = 0, std::string mode = "");

    ~PlotDrawer();
};

#endif // PLOTDRAWER_H
