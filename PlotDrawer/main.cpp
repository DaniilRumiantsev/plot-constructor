#include "plotdrawer.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PlotDrawer w(0, argv[1] != nullptr ? argv[1] : "");
    w.show();
    return a.exec();
}
